[[_TOC_]]

# Descripción

En este ejemplo encontraremos un circuito secuencial sencillo formado por un contador de 4 bits y un clock de periodo configurable.

**Autor:** Juan González Gómez (a.k.a. Obijuan)

# Diagrama en bloques

![Bloques](.images/Bloques.png)

El diagrama está formado por:
1. Un generador de reloj cuyo periodo en milisegundos es ajustable por el usuario
2. Un contador ascendente de 4 bits con su línea de reset conectada a 0.
3. Un bus de 4 líneas de salida, conectadas a los 4 LEDs de la placa.

# Cómo probar el ejemplo

1. Abrir **iCEstudio**
2. Abrir el archivo **2_EDU_CIAA_FPGA.ice**
3. Conectar la **EDU-CIAA-FPGA** a la PC
4. **Verificar** el diseño (usando **Ctrl+R** o el botón en la esquina inferior derecha)
5. **Sintetizar** el diseño (usando **Ctrl+B** o el botón en la esquina inferior derecha)
6. **Cargar** el diseño en la placa (usando **Ctrl+U** o el botón en la esquina inferior derecha)
7. Observar el comportamiento de los LEDs 0 a 3

# Yo te propongo...
1. Editar el período de la señal de clock (bloque "ms") para que la frecuencia sea de 10 Hz
2. Editar el período de la señal de clock (bloque "ms") para que la frecuencia sea de 1  Hz
3. Conectar la entrada **rst** del contador al botón 1, intercalando una compuerta NOT entre ambos:

![Propuesta](.images/Proupuesta1.png)

¿Qué ocurre al presionar el botón 1?

 4. Modificar el circuito para que sirva como un cronómetro que cuente durante cuántos segundos se mantiene presionado el botón 2. Mantener el botón 1 para reiniciar la cuenta. ¿Una ayuda?... alcanza con agregar dos compuertas (y una entrada para el nuevo botón, obviamente).

# Enlaces relacionados

 - [Descripción de circuitos secuenciales en VHDL](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/wikis/Descripci%C3%B3n-secuencial-en-VHDL)
 - [Descripción de circuitos secuenciales en Verilog](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/wikis/Descripci%C3%B3n-secuencial-en-Verilog)
 - [Descripción y simulación de un contador en VHDL](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/06-Contador_universal)

