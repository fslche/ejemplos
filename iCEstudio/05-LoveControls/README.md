[[_TOC_]]

# Descripción

En este último ejemplo se utiliza la interfaz serie para comunicar la EDU-CIAA-FPGA con un panel web, que nos permite tener mayor disponibilidad de entradas y salidas de los existentes en la placa.

** NOTA : Este ejemplo requiere tener Chrome o Chromium instalados en la PC**

**Autor:** Juan González Gómez (a.k.a. Obijuan)

# Diagrama en bloques

![Bloques](.images/Bloques.png)

El diagrama consta de:
1. Un interruptor y un pulsador virtuales que se conectan al pin Rx de la FPGA y conmutan dos LED de salida
2. Dos LEDs virtuales conectados al pin Tx de la FPGA para ser conmutados por dos pulsadores de la placa

# Cómo probar el ejemplo

## A - Descargar la configuración a la placa

Este proceso es similar a los ejemplos anteriores:
1. Abrir **iCEstudio**
2. Abrir el archivo **4_EDU_CIAA_FPGA.ice**
3. Conectar la **EDU-CIAA-FPGA** a la PC
4. **Verificar** el diseño (usando **Ctrl+R** o el botón en la esquina inferior derecha)
5. **Sintetizar** el diseño (usando **Ctrl+B** o el botón en la esquina inferior derecha)
6. **Cargar** el diseño en la placa (usando **Ctrl+U** o el botón en la esquina inferior derecha)
7. **Cerrar iCEstudio** - esto es importante para liberar el puerto serie

## B - Habilitar las caracteristicas experimentales de Chrome

1. Abrir **Google Chrome** (tambien puede ser Chromium)
2. Ir a la URL : chrome://flags/#enable-experimental-web-platform-features
3. Habilitar la opcion **Experimental Web Platform features**

![ChromeExperimental](.images/love3.png)

4. Nos pedira reiniciar Chrome - aceptar

## C - Ir al panel web
1. Abrir la siguiente URL : https://fpgawars.github.io/LOVE-FPGA/Releases/v0.1.0/CT9/panel.html

![LoveControls](.images/love1.png)

2. Con la placa conectada, presionar el boton **Connect** y elegir la opcion **Dual RS232-HS (ttyUSB1)**

![LoveConnect](.images/love2.png)

3. Presionar los botones **q** y **Q** del panel. ¿Qué ocurre en la placa?
4. Presionar los botones 1 y 4 de la placa. ¿Qué ocurre en el panel?

# Yo te propongo...
1. Modificar los LED de salida de la placa
2. Modificar los pulsadores de la placa que cambian el estado del panel web
3. Agregar dos LED mas, de forma tal que con 4 botones del panel web se puedan manejar los 4 LED de la placa
4. Agregar cuatro luces virtuales adicionales que reflejen el resultado de las operaciones AND, OR, XOR y XNOR entre los botones 1 y 4. 